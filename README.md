# 'PrimitiveShapes' Library #

Fork of extended RANSAC shape detection library used by the DURAARK project.

Available for research purposes only (as per original licensing).
