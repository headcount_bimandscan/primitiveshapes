#ifndef CONSTRUCTED_H_
#define CONSTRUCTED_H_

#include <pcl/point_types.h>
#include <Geometry/PCLTools.h>

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include "pcshapes/Plane.h"
#include "pcshapes/Cylinder.h"


namespace Constructed {

typedef ::pcl::PointNormal Point;
typedef ::Geometry::PCLTools<Point> Tools;
typedef ::Tools::CloudType Cloud;
typedef ::Tools::Idx Idx;
typedef ::Tools::IdxSet IdxSet;

typedef enum { PLANE, CYLINDER } PrimitiveType;

class Base {
	public:
		typedef std::shared_ptr<Base> Ptr;
		typedef std::weak_ptr<Base> WPtr;
		typedef std::shared_ptr<const Base> ConstPtr;
		typedef std::weak_ptr<const Base> ConstWPtr;

	public:
		Base() {}
		virtual ~Base() {}

		virtual PrimitiveType type() const = 0;

		virtual void init(Cloud::ConstPtr cloud, const IdxSet& samples) {
			MiscLib::Vector<pcshapes::Vec3f> samplePoints(2*samples.size());
			for (unsigned int i = 0; i < samples.size(); ++i) {
				auto idx = samples[i];
				Eigen::Vector3f pnt = cloud->points[idx].getVector3fMap();
				Eigen::Vector3f nrm = cloud->points[idx].getNormalVector3fMap();
				samplePoints[i] = pcshapes::Vec3f(pnt[0], pnt[1], pnt[2]);
				samplePoints[samples.size()+i] = pcshapes::Vec3f(nrm[0], nrm[1], nrm[2]);
			}
			init(samplePoints);
		}

		virtual IdxSet assign(Cloud::ConstPtr cloud, float epsilon, const IdxSet& exclude = IdxSet()) const = 0;

	protected:
		virtual void init(const MiscLib::Vector<pcshapes::Vec3f>& samples) = 0;
};


class Plane : public Base {
	public:
		typedef std::shared_ptr<Plane> Ptr;
		typedef std::weak_ptr<Plane> WPtr;
		typedef std::shared_ptr<const Plane> ConstPtr;
		typedef std::weak_ptr<const Plane> ConstWPtr;
		typedef Eigen::Hyperplane<float,3> Hyperplane;

	public:
		Plane() {}
		virtual ~Plane() {}

		PrimitiveType type() const { return PLANE; }

		Hyperplane& hyperplane() { return m_hyperplane; }
		const Hyperplane& hyperplane() const { return m_hyperplane; }

		IdxSet assign(Cloud::ConstPtr cloud, float epsilon, const IdxSet& exclude = IdxSet()) const {
			IdxSet subset(cloud->size());
			std::iota(subset.begin(), subset.end(), 0);
			subset = Algorithm::setDifference(subset, exclude);
			IdxSet result;
			for (const auto& idx : subset) {
				if (std::abs(m_hyperplane.signedDistance(cloud->points[idx].getVector3fMap())) <= epsilon) result.push_back(idx);
			}
			return result;
		}

	protected:
		void init(const MiscLib::Vector<pcshapes::Vec3f>& samples) {
			pcshapes::Plane plane;
			plane.InitAverage(samples);
			auto pos = plane.getPosition();
			auto nrm = plane.getNormal();
			m_hyperplane = Hyperplane(Eigen::Vector3f(nrm[0], nrm[1], nrm[2]), Eigen::Vector3f(pos[0], pos[1], pos[2]));
		}

	protected:
		Hyperplane m_hyperplane;
};


} // Constructed

#endif /* CONSTRUCTED_H_ */
