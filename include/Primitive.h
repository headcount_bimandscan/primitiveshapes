#ifndef PRIMITIVE_H_
#define PRIMITIVE_H_

#include <fstream>
#include <memory>
#include <set>
#include <utility>
#include <vector>

#include <boost/optional.hpp>
#include <boost/none.hpp>
using boost::optional;
using boost::none;

#include <pcl/point_cloud.h>
#include <Eigen/Geometry>

#include "pcshapes/PrimitiveShape.h"
#include "pcshapes/BitmapPrimitiveShape.h"
using MiscLib::RefCountPtr;

namespace pcshapes {

typedef enum { PLANE = 0, CYLINDER = 1, SPHERE = 2, CONE = 3 } PrimitiveType;

class Primitive {
	public:
		typedef std::shared_ptr<Primitive>         Ptr;
		typedef std::shared_ptr<const Primitive>   ConstPtr;
		typedef RefCountPtr<PrimitiveShape>        PrimRefPtr;
		typedef std::vector<int>                   IdxSet;
		typedef std::shared_ptr<BitmapInfo>        BitmapInfoPtr;
		typedef Eigen::AlignedBox<float,3>         BBox;
		typedef std::vector<Eigen::Vector3f>       Polygon;
		typedef std::vector<Polygon>               Polygons;

	public:
		Primitive(PrimRefPtr shape, const IdxSet& indices, BitmapInfoPtr bitmapInfo, float epsilon, float bitmapEpsilon, const Polygons& polygons = Polygons());

		void init();

		virtual PrimitiveType type() const = 0;

		const IdxSet& indices() const;
		float         area() const;
		const BBox&   boundingBox() const;
		virtual float minCurvature() const = 0;
		virtual float maxCurvature() const = 0;
		virtual Eigen::Vector3f primaryDirection() const = 0;
		const Polygons& polygons() const;

		Eigen::Vector3f project(const Eigen::Vector2f& point) const;
		Eigen::Vector3f project(const Eigen::Vector3f& point) const;
		Eigen::Vector3f normal(const Eigen::Vector2f& position) const;
		Eigen::Vector3f normal(const Eigen::Vector3f& position) const;

		float signedDistance(const Eigen::Vector3f& point) const;
		float absDistance(const Eigen::Vector3f& point) const;
		//virtual float intersectionParameter(const Eigen::ParametrizedLine<float, 3>& line) = 0;

		std::shared_ptr<BitmapInfo> bitmapInfo();
		std::shared_ptr<const BitmapInfo> bitmapInfo() const;
		float bitmapEpsilon() const;

		char testPointInBitmap(const Eigen::Vector3f& pt);
		optional<unsigned int> linearBitmapIndex(const Eigen::Vector3f& pt, bool checkDistance = true) const;
		float overlap(const std::vector<Eigen::Vector3f>& pts, const Eigen::Affine3f& transform, bool checkDistance = true) const;
		float overlap(Primitive::ConstPtr other, const Eigen::Affine3f& transform, bool checkDistance) const;

		virtual Eigen::Vector3f position() const = 0;
		virtual Eigen::Vector3f approxCenter() const = 0;
		virtual optional<float> intersectionParameter(const Eigen::ParametrizedLine<float, 3>& line) = 0;

	protected:
		virtual float approximateArea() = 0;
		virtual BBox  computeBB() = 0;

	protected:
		PrimRefPtr     m_shape;
		IdxSet         m_indices;
		BitmapInfoPtr  m_bitmapInfo;
		float          m_epsilon;
		float          m_bitmapEpsilon;
		float          m_area;
		BBox           m_bbox;
		std::vector<Eigen::Vector3f> m_gridPoints;
		Polygons       m_polygons;
};

#include "Primitive.inl"

} // pcshapes

#endif /* PRIMITIVE_H_ */
