inline Primitive::Primitive(PrimRefPtr shape, const IdxSet& indices, BitmapInfoPtr bitmapInfo, float epsilon, float bitmapEpsilon, const Polygons& polygons) : m_shape(shape), m_indices(indices), m_bitmapInfo(bitmapInfo), m_epsilon(epsilon), m_bitmapEpsilon(bitmapEpsilon), m_polygons(polygons) {
}

inline void Primitive::init() {
	m_area = approximateArea();
	m_bbox = computeBB();
	// compute grid points
	m_gridPoints.clear();
	Eigen::Vector2f param;
	for (unsigned int i=0; i < m_bitmapInfo->uextent; ++i) {
		param[0] = static_cast<float>(i)*m_bitmapEpsilon + m_bitmapInfo->bbox.Min()[0];
		for (unsigned int j = 0; j < m_bitmapInfo->vextent; ++j) {
			unsigned int linIndex = j*m_bitmapInfo->uextent + i;
			if (m_bitmapInfo->bitmap[linIndex] <= 0) continue;
			param[1] = static_cast<float>(j)*m_bitmapEpsilon + m_bitmapInfo->bbox.Min()[1];
			m_gridPoints.push_back(project(param));
		}
	}
}

inline const Primitive::IdxSet& Primitive::indices() const {
	return m_indices;
}

inline float Primitive::area() const {
	return m_area;
}

inline const Primitive::Polygons& Primitive::polygons() const {
	return m_polygons;
}

inline const Primitive::BBox& Primitive::boundingBox() const {
	return m_bbox;
}

inline Eigen::Vector3f Primitive::project(const Eigen::Vector2f& pos) const {
	Vec3f p, n;
	dynamic_cast<const BitmapPrimitiveShape *>(m_shape.Ptr())->InSpace(pos[0], pos[1], &p, &n);
	return Eigen::Vector3f(p[0], p[1], p[2]);
}

inline Eigen::Vector3f Primitive::project(const Eigen::Vector3f& pos) const {
	Vec3f p(pos[0], pos[1], pos[2]), pp;
	dynamic_cast<const BitmapPrimitiveShape*>(m_shape.Ptr())->Project(p, &pp);
	return Eigen::Vector3f(pp[0], pp[1], pp[2]);
}

inline float Primitive::signedDistance(const Eigen::Vector3f& pos) const {
	Vec3f p(pos[0], pos[1], pos[2]);
	return dynamic_cast<const BitmapPrimitiveShape*>(m_shape.Ptr())->SignedDistance(p);
}

inline float Primitive::absDistance(const Eigen::Vector3f& pos) const {
	return std::abs(signedDistance(pos));
}

inline Eigen::Vector3f Primitive::normal(const Eigen::Vector2f& pos) const {
	Vec3f p, n;
	dynamic_cast<const BitmapPrimitiveShape*>(m_shape.Ptr())->InSpace(pos[0], pos[1], &p, &n);
	return Eigen::Vector3f(n[0], n[1], n[2]);
}

inline Eigen::Vector3f Primitive::normal(const Eigen::Vector3f& pos) const {
	std::pair<float, float> params;
	Vec3f p(pos[0], pos[1], pos[2]);
	dynamic_cast<const BitmapPrimitiveShape*>(m_shape.Ptr())->Parameters(p, &params);
	return normal(Eigen::Vector2f(params.first, params.second));
}

inline std::shared_ptr<BitmapInfo> Primitive::bitmapInfo() {
	return m_bitmapInfo;
}

inline std::shared_ptr<const BitmapInfo> Primitive::bitmapInfo() const {
	return m_bitmapInfo;
}

inline float Primitive::bitmapEpsilon() const {
	return m_bitmapEpsilon;
}

inline char Primitive::testPointInBitmap(const Eigen::Vector3f& pt) {
	Vec3f p(pt.data());

	std::pair<float, float> param;
	const BitmapPrimitiveShape* prim = reinterpret_cast<const BitmapPrimitiveShape*>(m_shape.Ptr());
	prim->Parameters(p, &param);

	std::pair<int, int> inBitmap;
	prim->InBitmap(param, m_bitmapEpsilon, m_bitmapInfo->bbox, m_bitmapInfo->uextent, m_bitmapInfo->vextent, &inBitmap);

	if (inBitmap.first < 0 || inBitmap.first > static_cast<int>(m_bitmapInfo->uextent - 1) || inBitmap.second < 0 || inBitmap.second > static_cast<int>(m_bitmapInfo->vextent - 1)) return -128;

	/*
	inBitmap.first = GfxTL::Math< int >::Clamp(inBitmap.first, 0, m_bitmapInfo->uextent - 1);
	inBitmap.second = GfxTL::Math< int >::Clamp(inBitmap.second, 0, m_bitmapInfo->vextent - 1);
	*/

	unsigned int linIndex = inBitmap.first + inBitmap.second * m_bitmapInfo->uextent;

	return m_bitmapInfo->bitmap[linIndex];
}

inline optional<unsigned int> Primitive::linearBitmapIndex(const Eigen::Vector3f& pt, bool checkDistance) const {
	if (checkDistance && absDistance(pt) > m_epsilon) return none;

	Vec3f p(pt.data());
	std::pair<float, float> param;
	const BitmapPrimitiveShape* prim = reinterpret_cast<const BitmapPrimitiveShape*>(m_shape.Ptr());
	prim->Parameters(p, &param);

	std::pair<int, int> inBitmap;
	prim->InBitmap(param, m_bitmapEpsilon, m_bitmapInfo->bbox, m_bitmapInfo->uextent, m_bitmapInfo->vextent, &inBitmap);

	if (inBitmap.first < 0 || inBitmap.first > static_cast<int>(m_bitmapInfo->uextent - 1) || inBitmap.second < 0 || inBitmap.second > static_cast<int>(m_bitmapInfo->vextent - 1)) return none;

	return inBitmap.first + inBitmap.second * m_bitmapInfo->uextent;
//return m_bitmapInfo->bitmap[linIndex] > 0 ? 1 : 0;
}

inline float Primitive::overlap(const std::vector<Eigen::Vector3f>& pts, const Eigen::Affine3f& transform, bool checkDistance) const {
	std::vector<char> overlapBitmap(m_bitmapInfo->uextent * m_bitmapInfo->vextent, 0);

	for (const auto& pt : pts) {
		auto idx = linearBitmapIndex(transform * pt, checkDistance);
		if (!idx) continue;
		if (m_bitmapInfo->bitmap[idx.get()] > 0) overlapBitmap[idx.get()] = 1;
	}

	unsigned int overall = 0, overlap = 0;
	for (unsigned int i=0; i<(m_bitmapInfo->uextent * m_bitmapInfo->vextent); ++i) {
		if (m_bitmapInfo->bitmap[i] > 0) ++overall;
		if (overlapBitmap[i] > 0) ++overlap;
	}

	return (overall > 0) ? (static_cast<float>(overlap) / overall) : 0;
}

inline float Primitive::overlap(Primitive::ConstPtr other, const Eigen::Affine3f& transform, bool checkDistance) const {
	auto invT = transform.inverse();
	float o2t = other->overlap(m_gridPoints, transform, checkDistance);
	float t2o = overlap(other->m_gridPoints, invT, checkDistance);
	return std::min(o2t, t2o);
	//return other->overlap(m_gridPoints, transform, checkDistance);
}
