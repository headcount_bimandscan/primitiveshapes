#ifndef PRIMITIVECYLINDER_H_
#define PRIMITIVECYLINDER_H_

#include "Primitive.h"
#include "pcshapes/CylinderPrimitiveShape.h"

namespace pcshapes {

class PrimitiveCylinder : public Primitive {
	public:
		typedef std::shared_ptr<PrimitiveCylinder> Ptr;
		typedef std::weak_ptr<PrimitiveCylinder>   WPtr;

	public:
		PrimitiveCylinder(PrimRefPtr shape, const IdxSet& indices, BitmapInfoPtr bitmapInfo, float epsilon, float bitmapEpsilon, const Polygons& polygons = Polygons());

		PrimitiveType type() const;

		Eigen::Vector3f baseCenter() const;
		Eigen::Vector3f direction() const;
		float           radius() const;
		float           height() const;
		float           minCurvature() const;
		float           maxCurvature() const;
		Eigen::Vector3f primaryDirection() const;

		Eigen::Vector3f position() const;
		Eigen::Vector3f approxCenter() const;
		optional<float> intersectionParameter(const Eigen::ParametrizedLine<float, 3>& line);

	protected:
		float approximateArea();
		BBox  computeBB();

	protected:
		Eigen::Vector3f m_baseCenter;
		Eigen::Vector3f m_direction;
		float           m_radius;
		float           m_height;
		float           m_area;
};

#include "PrimitiveCylinder.inl"

} // pcshapes

#endif /* PRIMITIVECYLINDER_H_ */
