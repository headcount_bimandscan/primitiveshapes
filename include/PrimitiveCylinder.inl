inline PrimitiveCylinder::PrimitiveCylinder(PrimRefPtr shape, const IdxSet& indices, BitmapInfoPtr bitmapInfo, float epsilon, float bitmapEpsilon, const Polygons& polygons) : Primitive(shape, indices, bitmapInfo, epsilon, bitmapEpsilon, polygons) {
	CylinderPrimitiveShape* s = dynamic_cast<CylinderPrimitiveShape*>(shape.Ptr());
	auto& c = s->Internal();

	m_baseCenter = Eigen::Vector3f(c.AxisPosition()[0], c.AxisPosition()[1], c.AxisPosition()[2]);
	m_direction = Eigen::Vector3f(c.AxisDirection()[0], c.AxisDirection()[1], c.AxisDirection()[2]);
	m_radius = c.Radius();

	float l0 = s->MinHeight();
	float l1 = s->MaxHeight();
	m_height = l1-l0;
	m_baseCenter += l0*m_direction;
}

inline PrimitiveType PrimitiveCylinder::type() const {
	return CYLINDER;
}

inline Eigen::Vector3f PrimitiveCylinder::baseCenter() const {
	return m_baseCenter;
}

inline Eigen::Vector3f PrimitiveCylinder::direction() const {
	return m_direction;
}

inline float PrimitiveCylinder::radius() const {
	return m_radius;
}

inline float PrimitiveCylinder::height() const {
	return m_height;
}

inline float PrimitiveCylinder::minCurvature() const {
	return 0.f;
}

inline float PrimitiveCylinder::maxCurvature() const {
	return 1.f / m_radius;
}

inline Eigen::Vector3f PrimitiveCylinder::primaryDirection() const {
	return m_direction;
}

inline Eigen::Vector3f PrimitiveCylinder::position() const {
	return m_baseCenter;
}

inline Eigen::Vector3f PrimitiveCylinder::approxCenter() const {
	return m_baseCenter + 0.5f * m_height * m_direction;
}

inline optional<float> PrimitiveCylinder::intersectionParameter(const Eigen::ParametrizedLine<float, 3>& line) {
	Eigen::Vector3f origin = line.pointAt(0);
	Eigen::Vector3f target = line.pointAt(1);
	Eigen::Vector3f dir = target-origin;
	dir.normalize();
	CylinderPrimitiveShape* s = dynamic_cast<CylinderPrimitiveShape*>(m_shape.Ptr());
	auto cylinder = s->Internal();
	float first, second;
	unsigned int count = cylinder.Intersect(Vec3f(origin.data()), Vec3f(dir.data()), &first, &second);
	if (count) return first;
	return none;
}

inline float PrimitiveCylinder::approximateArea() {
	unsigned int pixelCount = 0;
	for (unsigned int v = 0; v < m_bitmapInfo->vextent; ++v) {
		for (unsigned int u = 0; u < m_bitmapInfo->uextent; ++u) {
			if (m_bitmapInfo->bitmap[u + v*m_bitmapInfo->uextent]) ++pixelCount;
		}
	}
	float factor = static_cast<float>(pixelCount) / (m_bitmapInfo->vextent * m_bitmapInfo->uextent);
	float cyl = 2.f * M_PI * m_radius * m_height;
	return factor * cyl;
}

inline PrimitiveCylinder::BBox PrimitiveCylinder::computeBB() {
	Eigen::Vector3f a = m_baseCenter;
	Eigen::Vector3f b = m_baseCenter + m_height*m_direction;
	float ax = std::min(a[0], b[0]), bx = std::max(a[0], b[0]);
	float ay = std::min(a[1], b[1]), by = std::max(a[1], b[1]);
	float az = std::min(a[2], b[2]), bz = std::max(a[2], b[2]);
	float dx = ax-bx, dy = ay-by, dz = az-bz;

	float dn = dx*dx+dy*dy+dz*dz;
	float kx = std::sqrt((dy*dy+dz*dz)/dn) * m_radius;
	float ky = std::sqrt((dx*dx+dz*dz)/dn) * m_radius;
	float kz = std::sqrt((dx*dx+dy*dy)/dn) * m_radius;

	BBox result;
	result.min() = Eigen::Vector3f( ax - kx, ay - ky, az - kz );
	result.max() = Eigen::Vector3f( bx + kx, by + ky, bz + kz );
	result.min() -= Eigen::Vector3f::Constant(m_epsilon);
	result.max() += Eigen::Vector3f::Constant(m_epsilon);
	return result;
}
