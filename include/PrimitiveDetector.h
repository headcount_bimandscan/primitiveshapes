#ifndef PRIMITIVEDETECTOR_H
#define PRIMITIVEDETECTOR_H

#include <bitset>
#include <numeric>

#include "pcshapes/RansacShapeDetector.h"
#include "pcshapes/PlanePrimitiveShapeConstructor.h"
#include "pcshapes/CylinderPrimitiveShapeConstructor.h"
#include "pcshapes/SpherePrimitiveShapeConstructor.h"
#include "pcshapes/GfxTL/HyperplaneCoordinateSystem.h"

#include "PrimitivePlane.h"
#include "PrimitiveCylinder.h"
#include "PrimitiveSphere.h"

namespace pcshapes {

typedef std::bitset<3> SupportedTypes; // (PLANE,CYLINDER,SPHERE)

class PrimitiveDetector {
	public:
		typedef std::shared_ptr<PrimitiveDetector> Ptr;

		typedef std::vector<int>  IdxSet;

	public:
		PrimitiveDetector();
		virtual ~PrimitiveDetector();

		float getEpsilon();
		float getBitmapEpsilon();
		float getNormalThreshold();
		unsigned int getMinimumSupport();
		float getProbability();

		void setEpsilon(float epsilon);
		void setBitmapEpsilon(float bitmapEpsilon);
		void setNormalThreshold(float normalThreshold);
		void setMinimumSupport(unsigned int minimumSupport);
		void setProbability(float probability);

		template<class PointType>
		std::vector<Primitive::Ptr> detectPrimitives(typename pcl::PointCloud<PointType>::ConstPtr cloud, SupportedTypes types = std::bitset<3>("111"), bool smooth = false, IdxSet subset = IdxSet());

	protected:
		void smoothBitmap(std::shared_ptr<BitmapInfo> bitmapInfo);

	protected:
		float m_epsilon;
		float m_bitmapEpsilon;
		float m_normalThreshold;
		unsigned int m_minimumSupport;
		float m_probability;
};

#include "PrimitiveDetector.inl"

} // pcshapes

#endif // PRIMITIVEDETECTOR_H
