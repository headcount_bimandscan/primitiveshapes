inline PrimitiveDetector::PrimitiveDetector() :	m_epsilon(0.02f),	m_bitmapEpsilon(0.2f), m_normalThreshold(0.8f), m_minimumSupport(100),m_probability(0.01f) {
}

inline PrimitiveDetector::~PrimitiveDetector() {
}

inline float PrimitiveDetector::getEpsilon() {
	return m_epsilon;
}

inline float PrimitiveDetector::getBitmapEpsilon() {
	return m_bitmapEpsilon;
}

inline float PrimitiveDetector::getNormalThreshold() {
	return m_normalThreshold;
}

inline unsigned int PrimitiveDetector::getMinimumSupport() {
	return m_minimumSupport;
}

inline float PrimitiveDetector::getProbability() {
	return m_probability;
}

inline void PrimitiveDetector::setEpsilon(float epsilon) {
	m_epsilon = epsilon;
}

inline void PrimitiveDetector::setBitmapEpsilon(float bitmapEpsilon) {
	m_bitmapEpsilon = bitmapEpsilon;
}

inline void PrimitiveDetector::setNormalThreshold(float normalThreshold) {
	m_normalThreshold = normalThreshold;
}

inline void PrimitiveDetector::setMinimumSupport(unsigned int minimumSupport) {
	m_minimumSupport = minimumSupport;
}

inline void PrimitiveDetector::setProbability(float probability) {
	m_probability = probability;
}

template <class PointType>
inline std::vector<Primitive::Ptr> PrimitiveDetector::detectPrimitives(typename pcl::PointCloud<PointType>::ConstPtr cloud, SupportedTypes types, bool smooth, IdxSet subset) {
	if (!subset.size()) {
		subset.resize(cloud->size());
		std::iota(subset.begin(), subset.end(), 0);
	}

	// copy pointcloud to pcshapes version
	PointCloud pc;
	pc.resize(subset.size());
	unsigned int pcIdx = 0;
	for (auto j : subset) {
		const auto& pt = cloud->points[j];
		pc[pcIdx].pos = Vec3f(pt.x, pt.y, pt.z);
		pc[pcIdx].normal = Vec3f(pt.normal_x, pt.normal_y, pt.normal_z);
		pc[pcIdx].originalIndex = j;
		++pcIdx;
	}

	GfxTL::AABox<Vec3f> bbox;
	bbox.Bound(pc, pc.size());
	pc.setBBox(bbox.Min(), bbox.Max());

	RansacShapeDetector::Options ransacOptions;
	ransacOptions.m_epsilon       = m_epsilon;
	ransacOptions.m_bitmapEpsilon = m_bitmapEpsilon;
	ransacOptions.m_normalThresh  = m_normalThreshold;
	ransacOptions.m_minSupport    = m_minimumSupport;
	ransacOptions.m_probability   = m_probability;

	RansacShapeDetector   detector(ransacOptions);
 	if (types.test(PLANE))    detector.Add(new PlanePrimitiveShapeConstructor());
 	if (types.test(CYLINDER)) detector.Add(new CylinderPrimitiveShapeConstructor());
 	if (types.test(SPHERE))   detector.Add(new SpherePrimitiveShapeConstructor());


	std::vector<Primitive::Ptr> resultPrimitives;
	MiscLib::Vector< std::pair< MiscLib::RefCountPtr< PrimitiveShape >, size_t > > shapes;
	detector.Detect(pc, 0, pc.size(), &shapes);

	size_t endIdx = pc.size();
	for (unsigned int s = 0; s < shapes.size(); ++s) {
		size_t shapePointCount = shapes[s].second;
		std::vector<int> indices;
		for (size_t i = endIdx-shapePointCount; i < endIdx; ++i) {
			indices.push_back(pc[i].originalIndex);
		}
		std::shared_ptr<BitmapInfo> bitmapInfo(new BitmapInfo());
		BitmapPrimitiveShape* shape = dynamic_cast<BitmapPrimitiveShape*>(shapes[s].first.Ptr());
		shape->BuildBitmap(pc, &m_bitmapEpsilon, IndexIterator(endIdx-shapePointCount), IndexIterator(endIdx), &bitmapInfo->params, &bitmapInfo->bbox, &bitmapInfo->bitmap, &bitmapInfo->uextent, &bitmapInfo->vextent, &bitmapInfo->bmpIdx);
		if (smooth) smoothBitmap(bitmapInfo);

		std::deque<PrimitiveShape::ComponentPolygons> componentPolygons;
		shape->TrimmingPolygons(pc, m_bitmapEpsilon, endIdx-shapePointCount, endIdx, &componentPolygons);
		std::vector<std::vector<Eigen::Vector3f>> polygons;
		for (const auto& cpolys : componentPolygons) {
			for (const auto& cpoly : cpolys) {
				std::vector<Eigen::Vector3f> polygon;
				for (const auto& bmpv : cpoly) {
					size_t i = bmpv[0];
					size_t j = bmpv[1];
					float u = static_cast<float>(i)*m_bitmapEpsilon + bitmapInfo->bbox.Min()[0];
					float v = static_cast<float>(j)*m_bitmapEpsilon + bitmapInfo->bbox.Min()[1];
					Vec3f p, n;
					shape->InSpace(u, v, &p, &n);
					polygon.push_back(Eigen::Vector3f(p[0], p[1], p[2]));
				}
				polygons.push_back(polygon);
			}
		}

		Primitive::Ptr prim;
		switch (shapes[s].first->Identifier()) {
			case 1:  prim = Primitive::Ptr(new PrimitiveSphere(shapes[s].first, indices, bitmapInfo, m_epsilon, m_bitmapEpsilon, polygons)); break;
			case 2:  prim = Primitive::Ptr(new PrimitiveCylinder(shapes[s].first, indices, bitmapInfo, m_epsilon, m_bitmapEpsilon, polygons)); break;
			default: prim = Primitive::Ptr(new PrimitivePlane(shapes[s].first, indices, bitmapInfo, m_epsilon, m_bitmapEpsilon, cloud, polygons));
		}
		prim->init();
		resultPrimitives.push_back(prim);

		endIdx -= shapePointCount;
	}

	return resultPrimitives;
}

inline void PrimitiveDetector::smoothBitmap(std::shared_ptr<BitmapInfo> bitmapInfo) {
	MiscLib::Vector<char> smoothedBitmap;
	smoothedBitmap.resize(bitmapInfo->uextent * bitmapInfo->vextent);
	int kernelDelta = std::ceil(0.2f / m_bitmapEpsilon);
	int kernelWidth = 2 * kernelDelta + 1;
	float kernel[kernelWidth];
	for (int i = 0; i < kernelWidth; ++i) kernel[i] = 1.f/kernelWidth;

	for (unsigned int v = 0; v < bitmapInfo->vextent; ++v) {
		for (unsigned int u = 0; u < bitmapInfo->uextent; ++u) {
			float val = 0.f;
			for (int dv = -kernelDelta; dv <= kernelDelta; ++dv) {
				for (int du = -kernelDelta; du <= kernelDelta; ++du) {
					int absU = GfxTL::Math< int >::Clamp(static_cast<int>(u) + du, 0, bitmapInfo->uextent-1);
					int absV = GfxTL::Math< int >::Clamp(static_cast<int>(v) + dv, 0, bitmapInfo->vextent-1);

					val += kernel[du+kernelDelta] * kernel[dv+kernelDelta] * (bitmapInfo->bitmap[absU + absV * bitmapInfo->uextent] ? 1.f : 0.f);
				}
			}
			if (val > 1.f) val = 1.f;
			smoothedBitmap[u + v * bitmapInfo->uextent] = -128 + 255 * val;
		}
	}
	bitmapInfo->bitmap = smoothedBitmap;
}
