#ifndef PRIMITIVEPLANE_H_
#define PRIMITIVEPLANE_H_

#include "Primitive.h"

#include "pcshapes/PlanePrimitiveShape.h"

namespace pcshapes {

class PrimitivePlane : public Primitive {
	public:
		typedef std::shared_ptr<PrimitivePlane> Ptr;
		typedef std::weak_ptr<PrimitivePlane>   WPtr;
		typedef Eigen::Hyperplane<float,3>      Hyperplane;

	public:
		template <class CloudPtr>
		PrimitivePlane(PrimRefPtr shape, const IdxSet& indices, BitmapInfoPtr bitmapInfo, float epsilon, float bitmapEpsilon, CloudPtr cloud, const Polygons& polygons = Polygons());

		PrimitiveType type() const;

		float minCurvature() const;
		float maxCurvature() const;
		Eigen::Vector3f primaryDirection() const;

		Eigen::Vector3f normal();
		Eigen::Vector3f normal(const Eigen::Vector2f& position);
		Eigen::Vector3f normal(const Eigen::Vector3f& position);

		float offset() const;
		const Eigen::Matrix3f& hcs() const;

		const Hyperplane& hyperplane();
		Eigen::Vector3f position() const;
		Eigen::Vector3f approxCenter() const;
		optional<float> intersectionParameter(const Eigen::ParametrizedLine<float, 3>& line);

	protected:
		float approximateArea();
		BBox  computeBB();

	protected:
		Hyperplane      m_plane;
		Eigen::Vector3f m_planePosition;
		Eigen::Matrix3f m_hcs;
};

#include "PrimitivePlane.inl"

} // pcshapes

#endif /* PRIMITIVEPLANE_H_ */
