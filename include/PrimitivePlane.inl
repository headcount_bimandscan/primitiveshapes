template <class CloudPtr>
inline PrimitivePlane::PrimitivePlane(PrimRefPtr shape, const IdxSet& indices, BitmapInfoPtr bitmapInfo, float epsilon, float bitmapEpsilon, CloudPtr cloud, const Polygons& polygons) : Primitive(shape, indices, bitmapInfo, epsilon, bitmapEpsilon, polygons) {
	PlanePrimitiveShape* ps = dynamic_cast<PlanePrimitiveShape*>(shape.Ptr());
	const Plane& plane = ps->Internal();
	auto planePosition = plane.getPosition();

	auto hcs = ps->getHCS();
	m_hcs.col(0) = Eigen::Vector3f(hcs[0][0], hcs[0][1], hcs[0][2]);
	m_hcs.col(1) = Eigen::Vector3f(hcs[1][0], hcs[1][1], hcs[1][2]);
	m_hcs.col(2) = Eigen::Vector3f(hcs[2][0], hcs[2][1], hcs[2][2]);

	Eigen::Vector3f normalSum(0.f, 0.f, 0.f);
	for (const auto& idx : indices) {
		normalSum += cloud->points[idx].getNormalVector3fMap();
	}
	auto planeNormal = plane.getNormal();
	float planeDist = -plane.SignedDistToOrigin();
	if (normalSum.dot(Eigen::Vector3f(planeNormal[0], planeNormal[1], planeNormal[2])) < 0.f) {
		planeNormal *= -1.f;
		planeDist *= -1.f;
	}
	m_plane = Hyperplane(Eigen::Vector3f(planeNormal[0], planeNormal[1], planeNormal[2]), planeDist);
	m_planePosition = Eigen::Vector3f(planePosition[0], planePosition[1], planePosition[2]);
}

inline PrimitiveType PrimitivePlane::type() const {
	return PLANE;
}

inline float PrimitivePlane::minCurvature() const {
	return 0.f;
}

inline float PrimitivePlane::maxCurvature() const {
	return 0.f;
}

inline Eigen::Vector3f PrimitivePlane::primaryDirection() const {
	return m_plane.normal();
}

inline Eigen::Vector3f PrimitivePlane::normal() {
	return m_plane.normal();
}

inline Eigen::Vector3f PrimitivePlane::normal(const Eigen::Vector2f&) {
	return m_plane.normal();
}

inline Eigen::Vector3f PrimitivePlane::normal(const Eigen::Vector3f&) {
	return m_plane.normal();
}

inline float PrimitivePlane::offset() const {
	return m_plane.offset();
}

inline const Eigen::Matrix3f& PrimitivePlane::hcs() const {
	return m_hcs;
}

inline const PrimitivePlane::Hyperplane& PrimitivePlane::hyperplane() {
	return m_plane;
}

inline Eigen::Vector3f PrimitivePlane::position() const {
	return m_planePosition;
}

inline Eigen::Vector3f PrimitivePlane::approxCenter() const {
	Eigen::Vector2f param;
	float iu = 0.5f * m_bitmapInfo->uextent;
	float iv = 0.5f * m_bitmapInfo->vextent;
	param[0] = iu*m_bitmapEpsilon + m_bitmapInfo->bbox.Min()[0];
	param[1] = iv*m_bitmapEpsilon + m_bitmapInfo->bbox.Min()[1];
	return project(param);
}

inline optional<float> PrimitivePlane::intersectionParameter(const Eigen::ParametrizedLine<float, 3>& line) {
	return line.intersectionParameter(m_plane);
}

inline float PrimitivePlane::approximateArea() {
	unsigned int pixelCount = 0;
	for (unsigned int v = 0; v < m_bitmapInfo->vextent; ++v) {
		for (unsigned int u = 0; u < m_bitmapInfo->uextent; ++u) {
			if (m_bitmapInfo->bitmap[u + v*m_bitmapInfo->uextent]) ++pixelCount;
		}
	}
	//auto& min = m_bitmapInfo->bbox.Min();
	//auto& max = m_bitmapInfo->bbox.Max();

	//Vec3f pMax, pMin, c, n;
	//m_shape->InSpace(min[0], min[1], &pMax, &n);
	//m_shape->InSpace(max[0], max[1], &pMin, &n);
	//m_shape->InSpace(max[0], min[1], &c, &n);

	return static_cast<float>(pixelCount * m_bitmapEpsilon * m_bitmapEpsilon);
}

inline PrimitivePlane::BBox PrimitivePlane::computeBB() {
	auto& min = m_bitmapInfo->bbox.Min();
	auto& max = m_bitmapInfo->bbox.Max();

	BBox result;
	Vec3f p, n;
	m_shape->InSpace(min[0], min[1], &p, &n);
	result.extend(Eigen::Vector3f(p[0], p[1], p[2]));
	m_shape->InSpace(max[0], max[1], &p, &n);
	result.extend(Eigen::Vector3f(p[0], p[1], p[2]));
	m_shape->InSpace(min[0], max[1], &p, &n);
	result.extend(Eigen::Vector3f(p[0], p[1], p[2]));
	m_shape->InSpace(max[0], min[1], &p, &n);
	result.extend(Eigen::Vector3f(p[0], p[1], p[2]));
	result.min() -= Eigen::Vector3f::Constant(m_epsilon);
	result.max() += Eigen::Vector3f::Constant(m_epsilon);
	return result;
}
