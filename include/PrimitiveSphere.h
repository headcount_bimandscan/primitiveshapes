#ifndef PRIMITIVESPHERE_H_
#define PRIMITIVESPHERE_H_

#include "Primitive.h"
#include "pcshapes/SpherePrimitiveShape.h"

namespace pcshapes {

class PrimitiveSphere : public Primitive {
	public:
		typedef std::shared_ptr<PrimitiveSphere> Ptr;
		typedef std::weak_ptr<PrimitiveSphere>   WPtr;

	public:
		PrimitiveSphere(PrimRefPtr shape, const IdxSet& indices, BitmapInfoPtr bitmapInfo, float epsilon, float bitmapEpsilon, const Polygons& polygons = Polygons());

		PrimitiveType type() const;

		Eigen::Vector3f center() const;
		float           radius() const;

		float           minCurvature() const;
		float           maxCurvature() const;

		Eigen::Vector3f primaryDirection() const;

		Eigen::Vector3f position() const;
		Eigen::Vector3f approxCenter() const;
		optional<float> intersectionParameter(const Eigen::ParametrizedLine<float, 3>& line);

	protected:
		float approximateArea();
		BBox  computeBB();

	protected:
		Eigen::Vector3f m_center;
		float           m_radius;
};

#include "PrimitiveSphere.inl"

} // pcshapes

#endif /* PRIMITIVESPHERE_H_ */
