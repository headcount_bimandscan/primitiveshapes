inline PrimitiveSphere::PrimitiveSphere(PrimRefPtr shape, const IdxSet& indices, BitmapInfoPtr bitmapInfo, float epsilon, float bitmapEpsilon, const Polygons& polygons) : Primitive(shape, indices, bitmapInfo, epsilon, bitmapEpsilon, polygons) {
	SpherePrimitiveShape* s = dynamic_cast<SpherePrimitiveShape*>(shape.Ptr());
	auto& c = s->Internal();
	m_center = Eigen::Vector3f(c.Center()[0], c.Center()[1], c.Center()[2]);
	m_radius = c.Radius();
}

inline PrimitiveType PrimitiveSphere::type() const {
	return SPHERE;
}

inline Eigen::Vector3f PrimitiveSphere::center() const {
	return m_center;
}

inline float PrimitiveSphere::radius() const {
	return m_radius;
}

inline float PrimitiveSphere::minCurvature() const {
	return 1.f / m_radius;
}

inline float PrimitiveSphere::maxCurvature() const {
	return 1.f / m_radius;
}

inline Eigen::Vector3f PrimitiveSphere::primaryDirection() const {
	return Eigen::Vector3f::Zero();
}

inline Eigen::Vector3f PrimitiveSphere::position() const {
	return m_center;
}

inline Eigen::Vector3f PrimitiveSphere::approxCenter() const {
	return m_center;
}

inline optional<float> PrimitiveSphere::intersectionParameter(const Eigen::ParametrizedLine<float, 3>& line) {
	Eigen::Vector3f oc = m_center - line.origin();
	float l2oc = oc.squaredNorm();
	if (l2oc < m_radius*m_radius) { // starts inside of the sphere
		float tca = oc.dot(line.direction());			// omit division if ray.d is normalized
		float l2hc = (m_radius*m_radius - l2oc) + tca*tca;  // division
		float lambda = tca + sqrt(l2hc);
		if (lambda > 0.f) return lambda;
	} else {
		float tca = oc.dot(line.direction());
		if (tca < 0) // points away from the sphere
			return none;
		float l2hc = (m_radius*m_radius - l2oc) + (tca*tca);	// division
		if (l2hc > 0) {
			float lambda = tca - sqrt(l2hc);
			if (lambda > 0.f) return lambda;
		}
	}
	return none;
}

inline float PrimitiveSphere::approximateArea() {
	unsigned int pixelCount = 0;
	for (unsigned int v = 0; v < m_bitmapInfo->vextent; ++v) {
		for (unsigned int u = 0; u < m_bitmapInfo->uextent; ++u) {
			if (m_bitmapInfo->bitmap[u + v*m_bitmapInfo->uextent]) ++pixelCount;
		}
	}
	float factor = static_cast<float>(pixelCount) / (m_bitmapInfo->vextent * m_bitmapInfo->uextent);
	float sph = 4.f * M_PI * m_radius * m_radius;
	return factor * sph;
}

inline PrimitiveSphere::BBox PrimitiveSphere::computeBB() {
	BBox result;
	result.min() = m_center - Eigen::Vector3f(m_radius, m_radius, m_radius);
	result.max() = m_center + Eigen::Vector3f(m_radius, m_radius, m_radius);
	result.min() -= Eigen::Vector3f::Constant(m_epsilon);
	result.max() += Eigen::Vector3f::Constant(m_epsilon);
	return result;
}
